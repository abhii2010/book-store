import { combineReducers } from 'redux';
import { SET_BOOKS, CLEAR_BOOKS } from '../actions';

function books(state = [], action) {
  switch(action.type) {
    case SET_BOOKS:
      return action.items;
    case CLEAR_BOOKS:
      return [];
    default:
      return state;
  }
}

const rootReducer = combineReducers({ books });

export default rootReducer;