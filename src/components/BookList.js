import React, { Component } from 'react';
import { connect } from 'react-redux';
import BookItem from './BookItem';

class BookList extends Component {
  render() {
    return (
      (this.props.books && this.props.books.length > 0) ?
      <div className="container-list">
        {
          this.props.books.map((book, index) => {
            return (
              <BookItem  
                key={index} id={index}
                book={book}
              />
            )
          })
        }
      </div>
      :
      this.props.recordCount == 0?
        <div><h4>No Record Found</h4></div>
      :
        <div></div>  
    )
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps, null)(BookList);