import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Media } from 'react-bootstrap';

class BookItem extends Component {
  render() {
    let book = this.props.book.best_book[0];
    
    return (
      <li className="col-md-4 ">
        <Media className="list-group-item">
          <Media.Left>
            <img className="list-item book-img" src={book.small_image_url} alt={book.title}/>
          </Media.Left>
          <Media.Body>
              <Media.Heading>
                <Link className="list-item" to={`detail/${this.props.id}`}><h4>{book.title}</h4></Link>
              </Media.Heading>
          </Media.Body>
        </Media>
      </li>
    )
  }
}

export default BookItem;