import React, { Component } from 'react';
import { parseString } from 'xml2js';
import { Media, Breadcrumb } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class AuthorDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {authorDetails: null};
  }
  
  getAuthor = (authorId) => {
    const key = "p68tdvv11fDIxVpQdzpbew";
    const url = `https://www.goodreads.com/author/show.xml?key=${key}&id=${authorId}`;
    var result;
    fetch(url, {
      method: 'GET'
    }).then(response => response.text())
    .then(data => { 
      parseString(data, function(err,res) {
          result = res;
      });
      this.setState({authorDetails: result.GoodreadsResponse.author[0]});
    });
  }

  componentDidMount() {
    let { id } = this.props.match.params;
    this.getAuthor(id);
  }

  render() {
    
    return (
      <div className="container-detail">
           { this.state.authorDetails ?
           <div>
              <Breadcrumb>
                  <Link className="list-item" to="/">Back to Search Page</Link>
              </Breadcrumb>
              <Media>
                  <Media.Left>
                      <img className="list-item" src={this.state.authorDetails.image_url[0]} alt={this.state.authorDetails.name[0]} />
                  </Media.Left>
                  <Media.Body>
                      <Media.Heading>{this.state.authorDetails.name[0]}</Media.Heading>
                      <p dangerouslySetInnerHTML={{__html:this.state.authorDetails.about[0]}} />
                      <p>
                          <b>HomeTown</b> : {this.state.authorDetails.hometown[0]}
                      </p>
                      <p>
                        <b>Influences</b>: <div dangerouslySetInnerHTML={{__html:this.state.authorDetails.influences[0]}} />
                      </p>
                      <p><b>Born at</b>: {this.state.authorDetails.born_at[0]}</p>
                  </Media.Body>
              </Media>
            </div>  
            :
            <div></div>
            }
      </div>
      
    );
  }
}


export default AuthorDetail;