import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Media, Breadcrumb } from 'react-bootstrap';
import StarRating from 'react-bootstrap-star-rating';
import { Link } from 'react-router-dom';
import { clearBooks } from '../actions';


class BookDetail extends Component {
    render() {
    if(this.props.books && this.props.books.length > 0) {
        let { id } = this.props.match.params;
        let book = this.props.books[id].best_book[0];
        let rating = parseFloat(this.props.books[id].average_rating[0]);
        let authorId = book.author[0].id[0]._;
        return (
            <div className="container-detail">
                <Breadcrumb>
                    <Link className="list-item" to="/">Back to Search Page</Link>
                </Breadcrumb>
                <Media>
                    <Media.Left>
                        <img className="list-item" src={book.image_url} alt={book.title} />
                    </Media.Left>
                    <Media.Body>
                        <Media.Heading>{book.title}</Media.Heading>
                        <p>
                        <Link className="list-item" to={`author/${authorId}`} >by {book.author[0].name}</Link>
                        </p>
                        <p>
                            <StarRating defaultValue={rating} min={0} max={5} step={0.5} />
                        </p>
                    </Media.Body>
                </Media>
            </div>
        )
    } else {
        return (<div>
            <Breadcrumb>
                <Link className="list-item" to="/">Back to Search Page</Link>
            </Breadcrumb>
        </div>);
    } 
  }
}

function mapStateToProps(state) {
   return state;
}

export default connect(mapStateToProps, { clearBooks })(BookDetail);