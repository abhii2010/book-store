import React, { Component } from 'react';
import { Form, FormGroup, FormControl, ControlLabel, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { setBooks, clearBooks } from '../actions';
import { parseString } from 'xml2js';
import loadImage from '../assets/loading.gif';

class SearchBooks extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      book: ""
    };
    this.loading = false;
  }

  componentDidMount() {
    console.log("component did mount");
    this.props.clearBooks();
  }

  search() {
    if(this.state.book && this.state.book !== ""){
      let bookText = this.state.book;
      this.loading = true;
      var books, result;
      const key = "p68tdvv11fDIxVpQdzpbew";
      const url = `https://www.goodreads.com/search.xml?key=${key}&q=${bookText}`;
      fetch(url, {
        method: 'GET'
      }).then(response => response.text())
      .then(data => { 
        parseString(data, function(err,res) {
          result = res; 
        });
        books = result.GoodreadsResponse.search[0].results[0].work;
        if(books && books.length > 0) {
          this.props.setBooks(books);
        } else {
          this.props.setRecordCount();
          this.props.clearBooks();
        }
        
        this.loading = false;
      })
    } else {
      this.props.setRecordCount();
      this.props.clearBooks();
    }  
  }

  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.search();
    }
  }

  render() {
    return (
      <div>
        <Form inline>
          <FormGroup>
            <FormControl 
              type="text" 
              placeholder="Harry Potter" onKeyPress={this._handleKeyPress}
              onChange={event => this.setState({ book: event.target.value })}
            />
          </FormGroup>
          {' '}
          <Button onClick={() => this.search()}>Search</Button>
        </Form>
        { this.loading === true ?
          <img src={loadImage} width="50px" />
          :
          <div></div>
        }
      </div>
    )
  }
}

export default connect(null, { setBooks, clearBooks })(SearchBooks);