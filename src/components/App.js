import React, { Component } from 'react';
import SearchBooks from './SearchBooks';
import BookList from './BookList';
import '../styles/index.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recordCount : -1
    };
  }

  setRecordCount = () => {
      this.setState({
        recordCount: 0
      });
  }
  
  render() {
    return (
      <div className="main">
        <h2 className="text-title">Book Store</h2>
        <SearchBooks recordCount={this.state.recordCount} setRecordCount={this.setRecordCount} />
        <BookList recordCount={this.state.recordCount} />
      </div>
    )
  }
}

export default App;