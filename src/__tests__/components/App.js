import React from 'react';
import { shallow, mount, render } from 'enzyme';
import App from '../../components/App';
import toJson from 'enzyme-to-json';
import configureStore from 'redux-mock-store'; // Smart components

const state = {
  recordCount : -1
};
const mockStore = configureStore();
const store = mockStore(state);
// describe what we are testing
describe('App Component', () => {
 
    // make our assertion and what we expect to happen 
    it('should render without throwing an error', () => {
      expect(shallow(<App />).find('div.main').exists()).toBe(true)
    });
});

describe('<App />', () => {
  describe('render()', () => {
    test('renders the component', () => {
      const wrapper = shallow(<App />);
      expect(toJson(wrapper)).toMatchSnapshot();
    });
  });
});

