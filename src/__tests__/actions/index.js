import configureStore from 'redux-mock-store'; // Smart components
import SET_BOOKS from '../../actions';

const mockStore = configureStore();
const store = mockStore(state);

describe('SET_BOOKS', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
            type: SET_BOOKS,
            items: {}
        },
      ];
  
      store.dispatch(selectActions.SET_BOOKS({}));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });