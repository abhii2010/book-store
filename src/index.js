import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import App from './components/App';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import BookDetail from './components/BookDetail';
import AuthorDetail from './components/AuthorDetail';

const store = createStore(rootReducer);
store.subscribe(() => console.log('store', store.getState()));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={App} />
        <Route exact path='/detail/:id' component={BookDetail} />
        <Route path='/detail/author/:id' component={AuthorDetail} />
      </Switch>
    </BrowserRouter>  
  </Provider>, document.getElementById('root'));
